package Watstock;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.util.ExitUtil;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.sql.*;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.util.CollectionAccumulator;
import scala.Tuple2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


public class App {
    enum Destination{
        RealTime,
        ToFiles
    }
    private static class ResolutionWindow{
        int writeThreshold;
        int resolution; // in seconds
        Duration resMill;
        boolean aggregate;
        Destination destination;

        private ResolutionWindow(PipelineConfig.Windows cfg, Destination dest){
            writeThreshold = (int)PipelineConfig.getBatchSize();
            resolution = cfg.resolution;
            resMill = new Duration(resolution * 1000);
            aggregate = cfg.aggregate;
            destination = dest;
        }

        private static ResolutionWindow createRTWindow(){
            ResolutionWindow window = new ResolutionWindow();
            window.destination = Destination.RealTime;
            window.resolution = 1;
            window.resMill = new Duration(window.resolution * 1000);
            window.aggregate = true;
            return window;
        }

        private ResolutionWindow(){
        }
    }

    private SparkSession spark;
    private JavaStreamingContext ssc;
    private PipelineConfig.Proxy proxy;
    private java.util.List<ResolutionWindow> windows;
    private Set<String> validTickers = null;

    private CollectionAccumulator<Row> socketOutput;

    private static final Duration secDuration = new Duration(1000);

    private static final Log LOG = LogFactory.getLog(App.class);

    private static String getS3Path(){
        String res = "intrinio-slices/FOLDER1/SS/T4/IntrinioStreamPipe/1.0/test";

        try{
            res =  Paths.get(PipelineConfig.getFileDestination()
                    ,PipelineConfig.getAppName()
                    ,PipelineConfig.getVersion()
            ).toString() + File.pathSeparator;
        }
        catch (Exception ex){
            LOG.error(ex);
        }
        return res;
    }


    private void process(JavaDStream<String> stream,  ResolutionWindow window){
        final int resolution = window.resolution;
        final int writeThreshold = window.writeThreshold;
        final PipelineConfig.OutputStream outSocket = PipelineConfig.getOutput();

        socketOutput.reset();

        stream.foreachRDD((VoidFunction2<JavaRDD<String>,Time>) (rdd , time) -> {

            Dataset<Row> jsonDS = null;
                try {
                    jsonDS = spark.read().json(rdd);
                }
                catch (Exception ex)
                {
                    LOG.error(ex);
                }

                if(jsonDS == null || !(Arrays.asList(jsonDS.columns()).contains("ticker")))
                    return;

                Dataset output = jsonDS.withColumnRenamed("size", "volume");

                output.persist();

                if (window.aggregate) {
                    Dataset dsWithCount = output.withColumn("data_points", functions.lit(1));
                    output = dsWithCount.groupBy("ticker", "type").agg(
                            functions.avg("volume").alias("avg_volume"),
                            functions.avg("price").alias("avg_price"),
                            functions.sum("data_points").alias("data_points"));
                    output = output.withColumn("mean", functions.lit(""));
                    output = output.withColumn("std_deviation", functions.lit(""));
                    output = output.withColumn("median", functions.lit(""));
                    output = output.withColumn("timestamp_utc", functions.lit(time.milliseconds()));
                }

                if (window.destination == Destination.ToFiles) {
                    output = output.withColumn("freq", functions.lit(String.valueOf(resolution) + "sec"));

                    final String s3Path = getS3Path();
                    LOG.info("Put to batch");

                    BatchSing.putToBatch(output, s3Path, writeThreshold);


                } else if (window.destination == Destination.RealTime) {
                    try {
                        final CollectionAccumulator toSock = output.sparkSession().sparkContext().collectionAccumulator("toSocket");
                        output.foreachPartition(part->{
                                while(part.hasNext())
                                    toSock.add(part.next());
                        });

                        List<Row> allAgg = toSock.value();

                        Server server = Server.getInstance(outSocket);

                        for (Row r : allAgg)
                            server.send(r);

                    }catch (Exception ex) {
                        LOG.error(ex);
                    }

                } else LOG.error("Unhandled destination");
            //}
        });
    }

    private void readTickerFile(java.nio.file.Path filePath){
        if (filePath == null)
            return;
        try {
            validTickers = new HashSet<>(Files.readAllLines( filePath)
                    .stream()
                    .flatMap(line -> Arrays.stream(line.split( ",")))
                    .map(String::trim)
                    .collect(Collectors.toSet()));
        }
        catch (IOException ex){
            Logger.getLogger(App.class).error(ex);
        }
    }


    private void printUsage(Options opts) {
        new HelpFormatter().printHelp("ApplicationMaster", opts);
    }



    private void run() throws YarnException, IOException {
        LOG.info("Starting ApplicationMaster");

        try {
            startSpark();
        } catch (Exception e) {
            LOG.error("Spark failed ");
        }
    }


    private boolean init(String[] args)throws  Exception {
        Options opts = new Options();
        opts.addOption("test_server", false, "Use test server");
        opts.addOption("cfg", true, "Config file");
        opts.addOption("log_properties", true, "log4j.properties file");

        opts.addOption("help", false, "Print usage");
        CommandLine cliParser = new GnuParser().parse(opts, args);

        String cofgFile = cliParser.getOptionValue("cfg", "");

        PipelineConfig.init(cofgFile);

        windows = PipelineConfig.getWindows().stream()
                .map(cfg -> new ResolutionWindow(cfg, Destination.ToFiles))
                .collect(Collectors.toList());

        windows.add(ResolutionWindow.createRTWindow());

        LOG.info("App NAme: " + PipelineConfig.getAppName());

        SparkConf sparkConf = new SparkConf().setAppName(PipelineConfig.getAppName())
                //.set("spark.eventLog.dir", "/mnt/watstock/pipe/out/lg.txt")
                //.set("spark.eventLog.enabled", "true", false)
                //.set("spark.sql.warehouse.dir", "hdfs:///var/usr/spark/", false)
                //.set("yarn.nodemanager.log-dirs", "/mnt/watstock/pipe/out/logs/", false)
                //.set("hadoop.root.logger", "INFO,console", false)
                //.set("yarn.log-aggregation-enable", )
                .setMaster("yarn")
                .set("spark.driver.host" , PipelineConfig.getHostIP(), false)
                //.set("spark.driver.port" , "82170", false)
                //.set("spark.driver.bindAddress" , "10.3.1.83", false)
                //.set("spark.rpc.numRetries" , "100", false)
                //.set("spark.rpc.askTimeout" , "100000", false)
                //.setSparkHome("/usr/lib/spark/")
                .setExecutorEnv("SPARK_YARN_MODE", "cluster")
                .setExecutorEnv("SPARK_HOME", "/usr/lib/spark/")
                //.set
                ;

        readTickerFile(PipelineConfig.getTickerFile());


        if (cliParser.hasOption("test_server"))
            proxy = PipelineConfig.getTestProxy();
        else
            proxy = PipelineConfig.getProxy();

                //JavaSparkSessionSingleton.getInstance(sparkConf);
        //SparkContext sc = spark.sparkContext();
        //JavaSparkContext jsc = JavaSparkContext.fromSparkContext(sc);
        //jsc.setLogLevel("debug");

        //sc.collectionAccumulator().register(sc, "outputStream");

        ssc = new JavaStreamingContext(sparkConf, secDuration);
        socketOutput = ssc.ssc().sparkContext().collectionAccumulator("toSocket");
        socketOutput.register$default$3();

        spark = SparkSession.builder().config(sparkConf).getOrCreate();

        //SparkSession spark = SparkSession.builder().config(ssc.sparkContext().getConf()).getOrCreate();

        LOG.info("REG: " +socketOutput.isRegistered());

        //Server.init(PipelineConfig.getOutput(), sparkConf);


        if (args.length == 0) {
            printUsage(opts);
            throw new IllegalArgumentException(
                    "No args specified for application master to initialize");
        }

        //Check whether customer log4j.properties file exists
        String log4jPath = cliParser.getOptionValue("log_properties");
        if (fileExist(log4jPath)) {
            try {
                Log4jPropertyHelper.updateLog4jConfiguration(App.class,
                        log4jPath);
            } catch (Exception e) {
                LOG.warn("Can not set up custom log4j properties. " + e);
            }
        }

        if (cliParser.hasOption("help")) {
            printUsage(opts);
            return false;
        }

        return true;
    }


    private static String getTicker(String jsonStr){
        if (jsonStr == null){
            LOG.warn("EMPTYJSON");
            return "no_val";
        }
        try {
            String[] halves = jsonStr.split("ticker\":\"");
            if (halves.length <= 1) {
                LOG.warn("Unexpected string: " + jsonStr);
                return "no_val";
            }

            return halves[1].split("\"", 2)[0];
        }catch (Exception ex){
            LOG.warn("NOTICKER: " + jsonStr);
        }
        return "no_val";
    }

    private void startSpark() throws  Exception{
        JavaReceiverInputDStream<String> dataStream = ssc.receiverStream(new CustomReceiver(proxy));

        if(PipelineConfig.getDebugOptions().enabled)
            dataStream.dstream().saveAsTextFiles(PipelineConfig.getDebugOptions().rawFilesDir.toString() , "txt");

        JavaDStream<String> records = dataStream.flatMap(record -> Arrays.asList(record.replace("}{", "}|{").split("\\|")).iterator());

        records = records.map(String::trim);

        JavaPairDStream<String, String> recordsPaired = records.mapToPair( rec -> new Tuple2<>(getTicker(rec), rec));

        final Set<String> tickers = validTickers;
        recordsPaired = recordsPaired.filter( record -> tickers.contains(record._1()));

        recordsPaired.print();

        JavaPairDStream<String, String> recordsPersist = recordsPaired.persist();

        windows.forEach(win->{
            JavaPairDStream<String, Iterable<String>> grouped =
                    recordsPersist.groupByKeyAndWindow(win.resMill, win.resMill,PipelineConfig.getPartitions());
            JavaDStream<String> collected = grouped.flatMap( x -> x._2.iterator() );
            process(collected, win);
        });


        ssc.start();
        ssc.awaitTermination();

        ssc.close();
        Server.getInstance(PipelineConfig.getOutput()).stop();
    }

    public static void main(String[] args) throws Exception {
        try {
            App appMaster = new App();
            //appMaster.init(args);


            LOG.info("Initializing ApplicationMaster");
            boolean doRun = appMaster.init(args);
            if (!doRun) {
                System.exit(0);
            }
            appMaster.run();
        } catch (Throwable t) {
            LOG.fatal("Error running ApplicationMaster", t);
            LogManager.shutdown();
            ExitUtil.terminate(1, t);
        }
    }

    private boolean fileExist(String filePath) {
        return new File(filePath).exists();
    }
}
