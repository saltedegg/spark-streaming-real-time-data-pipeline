package Watstock;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.spark.sql.Dataset;


public class UploadThread implements Runnable {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
    private Dataset batch = null;
    private String path = null;
    private static final Log LOG = LogFactory.getLog(UploadThread.class);
    static {
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public UploadThread(Dataset batch, String path) {
        this.batch = batch;
        this.path = path;
    }

    @Override
    public void run() {
        LOG.info("Start upload");
        long threadStartTime = System.nanoTime();
        DateFormat timeFormat = new SimpleDateFormat("[HH:mm:ss]");
        Time time = new Time(System.currentTimeMillis());
        LOG.info(timeFormat.format(time) + "  Start write.");

        DateFormat dateTime = new SimpleDateFormat("yyyyMMddHHmmss");

        Path finalPath = Paths.get( path, dateTime.format(new Date(System.currentTimeMillis())));

        String outStr = "s3n://" + finalPath.toString();

        LOG.info("Upload to dir: " +  outStr);

        try {
            batch.coalesce(1).write().partitionBy("freq", "ticker").mode("append").option("header", "true").csv(outStr );

        }catch (Exception ex){
            LOG.error(ex);
        }

        Time time2 = new Time(System.currentTimeMillis());
        LOG.info( timeFormat.format(time2) + "   Written.    " + timeFormat.format(time));
        // For debugging
        // System.out.println("Latency: " + (System.nanoTime() - threadStartTime));
    }
}
