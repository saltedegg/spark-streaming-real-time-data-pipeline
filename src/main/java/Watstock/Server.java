package Watstock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.spark.sql.Row;
import scala.Tuple2;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

public class Server
{
    class Session implements Serializable{
        Socket socket;
        Set tickers = null;

        private Session(Socket socket){
            this.socket = socket;
        }
    }

    static class OutputSocket implements Serializable{
        List<Session> sessions;
        OutputSocket(){
            sessions= new ArrayList<>();
        }
        OutputSocket(List<Session> cpy){
            sessions = cpy;
        }
        OutputSocket add(Session sess){
            sessions.add(sess);
            return this;
        }
    }

    private static final Log LOG = LogFactory.getLog(Server.class);

    private static Server singleton = null;
    private final int port;
    private boolean running;
    private final ExecutorService threadPool;
    private Thread serverThread;

    private int bufferSize = 65536;

    private static OutputSocket clients = new OutputSocket();

    private Server(int port)
    {

        try {
            System.out.println(InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.port = port;

        threadPool = Executors.newFixedThreadPool(10,new ThreadFactory()
        {
            private final AtomicInteger instanceCount = new AtomicInteger();
            @Override
            public Thread newThread(Runnable r)
            {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                t.setName("HANDLER_" + instanceCount.getAndIncrement());
                return t;
            }
        });
    }

    private static void init(int port){

        try {
            LOG.info("Server on: " + InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            LOG.error(e);
        }

        if (singleton != null){
            LOG.error("SEcond server is initialized");
        }

        LOG.info("Server Init");

        singleton = new Server(port);
        singleton.start();

        clients = new OutputSocket();
    }


    public static Server getInstance(PipelineConfig.OutputStream socket){
        if (singleton == null)
            init(socket.port);
        return singleton;
    }

    private void start()
    {
        running = true;
        // System.out.println("------------- Starting Server Up -------------");
        serverThread = new Thread(() ->
        {
            try
            {
                ServerSocket server = new ServerSocket(port);
                //server.setReuseAddress(true);

                LOG.info("iNNET: " + InetAddress.getLocalHost().getHostName());
                LOG.info("PORT: " + server.getLocalSocketAddress());
                LOG.info("PORT: " + server.getLocalPort());

                while ( running)
                {
                    final Socket client = server.accept();
                    LOG.info("Client bind attempt: "+ client.getInetAddress());

                    client.setKeepAlive(true);

                    Session session = new Session(client);

                    clients.add(session);
                   LOG.info("Client bind attempt: "+ client.getInetAddress());

                    threadPool.submit(() ->
                    {
                        try
                        {
                            while (running) {
                                if (session.socket.getInputStream().available() < 2) {
                                    sleep(1000);
                                }
                                else {

                                    BufferedReader in = new BufferedReader(new InputStreamReader(session.socket.getInputStream()));
                                    StringBuilder tickers = new StringBuilder();
                                    char[] buffer = new char[bufferSize];
                                    int len;
                                    while ((len = in.read(buffer,0, bufferSize)) != 0) {
                                        tickers.append(String.valueOf(buffer, 0, len));
                                        if (session.socket.getInputStream().available() < 2)
                                            break;
                                    }
                                    session.tickers = new HashSet<>(Arrays.asList(tickers.toString().split(",")));
                                }
                            }
                            }
                            catch (IOException | InterruptedException ex)
                            {
                                LOG.error(ex);
                            }

                    });
                }
            }
            catch (IOException ex)
            {
                LOG.error(ex);
            }
        });
        serverThread.setName("LISTENER");
        serverThread.start();
    }


    public void send(Tuple2<String , String> data){
        String ticker = data._1;
        String records = data._2;
        send(ticker, records);

    }

    public void send(List<Row> row){
        row.forEach(this::send);
    }

    public void send(Row row){
        String form = "{\"ticker\":%s,\"type\":%s,\"data_points\":%d,\"avg_price\":%f,\"avg_volume\":%f}";
        int tickerIdx = row.fieldIndex("ticker");
        int dataPtIdx = row.fieldIndex("data_points");
        int typeIdx = row.fieldIndex("type");
        int avgPriceIdx = row.fieldIndex("avg_price");
        int avgSizeIdx = row.fieldIndex("avg_volume");
        String output = String.format(form,
                row.getString(tickerIdx),
                row.getString(typeIdx),
                Integer.valueOf(row.get(dataPtIdx).toString()),
                row.getDouble(avgPriceIdx),
                row.getDouble(avgSizeIdx));

        String ticker = row.getString(tickerIdx);
        send(ticker, output );
    }

    private void send(String ticker, String record){
        HashSet<Session> toBeRemoved = new HashSet<>();

        clients.sessions.forEach((client) -> {
                    if (client.tickers == null || client.tickers.contains(ticker)){
                        try {
                            final PrintWriter out = new PrintWriter(client.socket.getOutputStream(), true);
                            out.println(record);
                            out.flush();
                        }catch (IOException ex) {
                            LOG.error(ex);
                            try {
                                toBeRemoved.add(client);
                                client.socket.close();
                            } catch (IOException ex2) {
                                LOG.error(ex2);
                            }
                        }
                    }
                }
        );
        if(!toBeRemoved.isEmpty()) {
            List<Session> temp = clients.sessions;
            temp.removeAll(toBeRemoved);
            clients = new OutputSocket(temp);
        }
    }


    void stop()
    {
        clients.sessions.forEach(x -> {
            try {
                x.socket.close();
            } catch (IOException e) {
                LOG.warn(e);
            }
        });
        running = false;
        if ( serverThread != null)
        {
            serverThread.interrupt();
        }
        threadPool.shutdown();
        serverThread = null;

    }

    public static void main(String[] args)
    {
        Server server = new Server(2003);
        server.start();
    }

}