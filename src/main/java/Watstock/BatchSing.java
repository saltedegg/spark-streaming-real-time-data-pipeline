package Watstock;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.types.StructType;


class BatchCollector {
    private Dataset batch = null;
    private int counter = 0;
    private final int writeThreshold;
    private final String filePath;
    private static final Log LOG = LogFactory.getLog(BatchSing.class);

    BatchCollector(int writeThreshold_, String path){

        writeThreshold = writeThreshold_;
        filePath = path;
    }

    void putInBatch(Dataset part) {
        try {
            if (batch == null)
                batch = part;
            else
                batch.union(part);

            counter += part.count();

            if (counter > writeThreshold) {
                LOG.info("Start writing to S3");

                Runnable runnable = new UploadThread(batch, filePath);
                Thread upload = new Thread(runnable);
                upload.setName("UploadThread");
                upload.start();

                counter = 0;
                batch = null;
            }

        }catch (Exception ex)
        {
            LOG.error(ex);
        }
    }

}

public class BatchSing{
    private static BatchSing instance = null;

    private Map<StructType, BatchCollector> batches;


    BatchSing(){
        batches = new HashMap<>();
    }

    private static BatchSing getInstance(){
        if(instance == null)
            instance = new BatchSing();
        return instance;
    }

    public static void putToBatch(Dataset partition, String finalPath, int threshold){
        if(partition == null)
            return;
        BatchSing sing = getInstance();
        if (! sing.batches.containsKey(partition.schema()))
            sing.batches.put(partition.schema(), new BatchCollector(threshold, finalPath));

        sing.batches.get(partition.schema()).putInBatch(partition);
    }
}
