package Watstock;

import com.google.common.io.Closeables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

public class CustomReceiver extends Receiver<String> {
    private int port = -1;
    private String host = null;
    private final int buffer_size;
    private static final Log LOG = LogFactory.getLog(CustomReceiver.class);

    public CustomReceiver( PipelineConfig.Proxy cfg) {
        super(StorageLevel.MEMORY_ONLY());
        port = cfg.port;
        host = cfg.host;
        buffer_size = cfg.bufferSize;

    }

    public CustomReceiver( String host_, int port_) {
        super(StorageLevel.MEMORY_AND_DISK_2());
        port = port_;
        host = host_;
        buffer_size = 65536;
    }

    public void onStart() {
        // Start the thread that receives data over a connection
        new Thread(() -> receive()).start();
    }

    public void onStop() {
        // There is nothing much to do as the thread calling receive()
        // is designed to stop by itself isStopped() returns false
    }

    /* Create a socket connection and receive data until receiver is stopped */
    private void receive() {
        try {
            Socket socket = null;
            BufferedReader reader = null;
            int len;
            try {
                //String ip = InetAddress.getByName(host).getHostAddress();
                LOG.info("SELFADDR: " + host);
                socket = new Socket(host, port);
                // connect to the server
                char[] buffer = new char[buffer_size];
                int tailLen = 0;
                reader = new BufferedReader(
                        new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));


                // Until stopped or connection broken continue reading
                while (!isStopped() && (len = reader.read(buffer,tailLen, buffer_size - tailLen)) > 0) {
                    len += tailLen;
                    tailLen = 0;
                    String str;
                    if (buffer[len - 1] != '}') {
                        int j;
                        for (j = len - 1; j >=0; j--)
                            if (buffer[j] == '}')
                                break;
                        char [] newBuff = new char[buffer_size];
                        tailLen = len - j - 1;
                        System.arraycopy(buffer, j + 1, newBuff, 0, tailLen);
                        str = String.valueOf(buffer, 0,  j + 1);
                        buffer = newBuff;
                    }
                    else
                        str = String.valueOf(buffer, 0, len);

                    //System.out.println(str);
                    store(str);
                }
            } finally {
                Closeables.close(reader, /*swallowIOException = */true);
                Closeables.close(socket,  /* swallowIOException = */ true);
            }
            // Restart in an attempt to connect again when server is active again
            restart("Trying to connect again");
        } catch(ConnectException ce) {
            // restart if could not connect to server
            restart("Could not connect", ce);
        } catch(Throwable t) {
            restart("Error receiving data", t);
        }
    }
}
