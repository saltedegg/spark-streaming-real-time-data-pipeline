package Watstock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PipelineConfig {
    public class Proxy{
        String host;
        int port;
        int bufferSize;

        private Proxy(JsonNode json){
            host = json.get("host").asText();
            port = json.get("port").asInt();
            bufferSize = json.get("buffer_size").asInt();
        }
    }

    public class Windows{
        int resolution;
        boolean aggregate;
        private Windows(JsonNode json){
            resolution = json.get("resolution(sec)").asInt();
            if(json.has("aggregate"))
                aggregate = json.get("aggregate").asBoolean();
            else
                aggregate = false;
        }
    }

    public class OutputStream{
        int port;
        int bufferSize;

        private OutputStream(JsonNode json){
            port = json.get("port").asInt();
            bufferSize = json.get("buffer_size").asInt();
        }
    }

    public class Debug{
        boolean enabled = false;
        Path rawFilesDir;
        Debug(JsonNode json){
            enabled = json.get("enable").asBoolean();
            rawFilesDir = Paths.get(json.get("write_raw_file").asText());
        }
        Debug(){
            enabled = false;
            rawFilesDir = null;
        }
    }

    private static PipelineConfig instance;

    private Proxy proxy;
    private List<Windows> windows = new ArrayList<>();
    private double batchSize;
    private OutputStream output;

    private Proxy testProxy;

    private String appName;
    private org.apache.log4j.Level loggerLevel;

    private String fileDestination;
    private String version;

    private Debug debug;
    private Path tickerFile;

    private int partitions;
    private String hostIP;

    static void init(String cfgFile){
        assert (instance == null);
        instance = new PipelineConfig(cfgFile);
    }

    private PipelineConfig(String cfgFile){
        assert (instance == null);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode cfg = mapper.readTree(new FileReader(cfgFile));
            proxy = new Proxy(cfg.get("intrinio_proxy"));
            output = new OutputStream(cfg.get("output_stream"));
            cfg.get("windows").elements().forEachRemaining(win -> windows.add(new Windows(win)));

            batchSize = cfg.get("batch_size").asDouble();

            if (cfg.has("test_proxy"))
                testProxy = new Proxy(cfg.get("test_proxy"));

            appName = cfg.get("app_name").asText();
            loggerLevel = org.apache.log4j.Level.toLevel(cfg.get("logger").asText());
            fileDestination = cfg.get("file_destination").asText();
            version = cfg.get("version").asText();
            if(cfg.has("debug"))
                debug = new Debug(cfg.get("debug"));
            else
                debug = new Debug();
            if(cfg.has("ticker_file"))
                tickerFile = Paths.get(cfg.get("ticker_file").asText());
            else
                tickerFile = null;

            partitions = cfg.get("partitions").asInt();
            hostIP = cfg.get("host_ip").asText();
        }catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static OutputStream getOutput() {
        assert (instance != null);
        return instance.output;
    }

    public static Proxy getProxy() {
        assert (instance != null);
        return instance.proxy;
    }

    public static List<Windows> getWindows() {
        assert (instance != null);
        return instance.windows;
    }

    public static Proxy getTestProxy() {
        assert (instance != null);
        return instance.testProxy;
    }

    public static org.apache.log4j.Level getLoggerLevel() {
        assert (instance != null);
        return instance.loggerLevel;
    }

    public static String getAppName() {
        assert (instance != null);
        return instance.appName;
    }

    public static String getFileDestination() {
        assert (instance != null);
        return instance.fileDestination;
    }

    public static String getVersion() {
        assert (instance != null);
        return instance.version;
    }

    public static Debug getDebugOptions(){
        assert (instance != null);
        return instance.debug;
    }

    public static Path getTickerFile(){
        assert (instance != null);
        return instance.tickerFile;
    }

    public static int getPartitions() {
        assert (instance != null);
        return instance.partitions;
    }

    public static double getBatchSize() {
        assert (instance != null);
        return instance.batchSize;
    }

    public static String getHostIP() {
        assert (instance != null);
        return instance.hostIP;
    }
}
